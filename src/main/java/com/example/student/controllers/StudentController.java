package com.example.student.controllers;

import com.example.student.model.tables.pojos.Students;
import com.example.student.repositories.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/students")
public class StudentController {
    private final StudentRepository repository;

    @GetMapping("/viewAll")
    public List<Students> viewAll() {
        return repository.displayAll();
    }

    @GetMapping("/viewOne/{id}")
    public Students viewOne(@PathVariable Integer id) {
        return repository.displayOne(id);
    }

    @PostMapping("/save")
    public void save(@RequestBody Students student) {
        repository.save(student);
    }

    @PutMapping("/update/{id}")
    public void update(@RequestBody Students student, @PathVariable Integer id) {
        repository.update(student, id);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id) {
        repository.delete(id);
    }
}
