package com.example.student.repositories;

import com.example.student.model.tables.pojos.Students;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.student.model.Tables.STUDENTS;

@Repository
@RequiredArgsConstructor
public class StudentRepository {
    private final DSLContext db;


    public void save(Students students) {
        db.insertInto(STUDENTS, STUDENTS.NAME, STUDENTS.AGE, STUDENTS.CONTACT_NUMBER, STUDENTS.EMAIL)
                .values(students.getName(), students.getAge(), students.getContactNumber(), students.getEmail())
                .execute();
    }

    public List<Students> displayAll() {
        return db.selectFrom(STUDENTS).fetchInto(Students.class);
    }

    public Students displayOne(Integer id) {
        return db.selectFrom(STUDENTS).where(STUDENTS.ID.eq(id)).fetchOneInto(Students.class);
    }


    public void update(Students students, Integer id) {
        db.update(STUDENTS)
                .set(STUDENTS.NAME, students.getName())
                .set(STUDENTS.AGE, students.getAge())
                .set(STUDENTS.CONTACT_NUMBER, students.getContactNumber())
                .set(STUDENTS.EMAIL, students.getEmail())
                .where(STUDENTS.ID.eq(id)).execute();
    }

    public void delete(Integer id) {
        db.deleteFrom(STUDENTS).where(STUDENTS.ID.eq(id)).execute();
    }
}
