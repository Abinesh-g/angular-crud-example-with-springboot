import {Injectable} from '@angular/core';
import {Students} from "./models/Students";

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  id: number;
  name: string;
  age: number;
  contactNumber: string;
  email: string;

  constructor() {
  };

  set(student: Students) {
    this.id = student.id;
    this.name = student.name;
    this.age = student.age;
    this.contactNumber = student.contactNumber;
    this.email = student.email;
  }

  get(): StudentService {
    return this;
  }

}

