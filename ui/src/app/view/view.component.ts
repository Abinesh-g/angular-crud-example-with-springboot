import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Students} from "../models/Students";
import {StudentService} from "../student.service";

// @ts-ignore
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']

})
export class ViewComponent implements OnInit {

  private baseUrl = "http://localhost:8080/students";
  allStudents: Students[] = []; // FOR GET
  studentData: Students;        // FOR UPDATE


  constructor(private http: HttpClient, private studentService: StudentService) {
  }

  ngOnInit(): void {
    // @ts-ignore
    return this.http.get(this.baseUrl + "/viewAll").subscribe(
      data => {
        console.log(data);
        this.allStudents = [];
        for (let key in data) {
          this.allStudents.push(data[key]);
        }
        console.log(this.allStudents);
      });
  }

  delete(student: Students): void {
    this.http.delete(this.baseUrl + "/delete/" + student.id).subscribe(
      data => {
        alert("data deleted ==> " + student.name);
      });
    location.reload();
  }

  update(student: Students): void {
    this.studentData = student;
    this.studentService.set(student);
  }
}


