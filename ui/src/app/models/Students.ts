export class Students {
  id: number;
  name: string;
  age: number;
  contactNumber: string;
  email: string;
}
