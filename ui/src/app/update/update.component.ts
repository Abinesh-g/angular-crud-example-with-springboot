import {Component, OnInit} from '@angular/core';
import {StudentService} from "../student.service";
import {Students} from "../models/Students";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  private baseUrl = "http://localhost:8080/students";
  students: Students;

  name: string;
  age: number;
  contactNumber: string;
  email: string;

  constructor(private studentService: StudentService, private http: HttpClient, private router: Router) {
  }

  ngOnInit(): void {
    console.error("this.name  " + this.name);
    if (this.name == '' || this.name==null) {
      this.router.navigate(["/", "view"]);
    }
    // alert("inside update");
    console.log(this.studentService.get());
    this.students = this.studentService.get();
    this.name = this.students.name;
    this.age = this.students.age;
    this.contactNumber = this.students.contactNumber;
    this.email = this.students.email;


  }

  updateStudent(student: Students) {
    // alert("this.name ====>" + student.name);
    console.log(student);
    console.log(this.age);
    this.http.put(this.baseUrl + "/update/" + student.id, student).subscribe(data => {
      alert(student.name + " data updates")
    });
  }

}
