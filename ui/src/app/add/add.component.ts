import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  constructor(private http: HttpClient) {
  }

  private baseUrl = "http://localhost:8080/students";
  // student: Students;
  id: number = null;
  name: string = '';
  age: number = null;
  contactNumber: string = '';
  email: string = '';

  ngOnInit(): void {
  }

  save(): void {

    /*    this.student.name = this.name;
        this.student.age = this.age;
        this.student.contactNumber = this.contactNumber;
        this.student.email = this.email;*/
    this.http.post(this.baseUrl + "/save", {
      name: this.name, age: this.age,
      contactNumber: this.contactNumber,
      email: this.email
    }).subscribe(data => {
      alert(this.name + " details are saved")
    });
  }
}
