import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AddComponent} from "./add/add.component";
import {ViewComponent} from "./view/view.component";
import {UpdateComponent} from "./update/update.component";
import {AppComponent} from "./app.component";


const routes: Routes = [
  {path: '', component: AppComponent},
  {path: 'add', component: AddComponent},
  {path: 'view', component: ViewComponent},
  {path: 'update', component: UpdateComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
